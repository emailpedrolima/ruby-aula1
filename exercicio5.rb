=begin
Crie uma função que dado um array de inteiros, retorne um array apenas com os divisíveis por 3.
Ex: Entrada: [3, 6, 7, 8]
    Saída: [3, 6]
=end



def Divpor3(arr)
  mult3 = []
  for i in arr
    if i%3 == 0
      mult3 << i 
    end
  end
print(mult3)
end

Divpor3([3, 6, 7, 8])