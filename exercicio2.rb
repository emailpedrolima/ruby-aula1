=begin
Crie uma função que dado dois valores, retorne a divisão deles (a divisão deve retornar um float).
Ex: Entrada: 5, 2
    Saída: 2.5
=end



def divisao(x,y)
  
    result =  x/y.to_f
    return result  
  
end


puts(divisao(5,2))
