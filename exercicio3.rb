=begin
Crie uma função, que dado um hash como parâmetro, e retorne um array com todos os valores que estão no hash elevados ao quadrado.
Ex: Entrada: {:chave1 => 5, :chave2 => 30, :chave3 => 20}
    Saída: [25, 900, 400]
=end


def elevaAoQuadrado(x)
  array = []
  i = 0
  x.each do |chave,valor|
    
    array[i] = valor*valor
    i+=1
  end
  
  puts array
end     

elevaAoQuadrado( {:chave1 => 5, :chave2 => 30, :chave3 => 20})
