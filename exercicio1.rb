=begin
 Crie uma função que dado um array de arrays, imprima na tela a soma e a multiplicação de todos os valores. 
 Ex: Entrada: [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
    Impressão na tela: Soma: 39
   Impressão na tela: Multiplicação: 100800
=end



def somaEMultiplica(arr)
  soma = 0
  mult = 1
  for vet in arr
    for valor in vet
      soma+=valor
      mult*=valor
    end 
  end 
  puts("Soma: " , soma )
  puts("Multiplicação: " , mult )
end 

somaEMultiplica([[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]])